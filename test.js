const fs = require("fs")

let name_project = 'htpProd';

function changeName(fileToChange){
    let project_name = process.argv.slice(2)[0];
    fs.readFile(fileToChange, 'utf8', function(err, data){
        if(err) return console.log("erreur ", err)  
            var result = data.replace(name_project,project_name);
            fs.writeFile(fileToChange, result, 'utf8', function(err){
                if(err) return console.log(err)
                return;
            })
            return;
    })
    console.log(name_project + " changed with " + project_name)
}

changeName(__dirname + '/src/urls.json')