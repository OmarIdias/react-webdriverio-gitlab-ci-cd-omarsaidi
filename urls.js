module.exports = {
    local: "http://localhost:3000/",
    prod: 'https://omar-test-prod.herokuapp.com/',
    qa: 'https://omar-qa.herokuapp.com/'
}