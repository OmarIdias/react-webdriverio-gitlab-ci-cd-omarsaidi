import logo from './logo.svg';
import './App.css';
import url from './urls.json'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>Hello World</p>
        <p>
          {url.prod}
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
